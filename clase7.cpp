#include <iostream>
#include<cstring>

using namespace std;

void funcionX(int numeros[], int numeros2[]);
void funcionN(int *num);
void impNombre(string* nom);

int main(int argc, const char * argv[]) {
    int xxx[3];
    int xxx2[3];
    char nombre[] = {"Francisco"};
    string nombre2 = "David";

    funcionX(xxx, xxx2);
    cout << "0: " << xxx2[0] << ", 1: " << xxx2[1] << ", 2: " << xxx2[2] << endl;

    int numero = 0;
    printf("Posicion No1 = %x\n", &numero);
    funcionN(&numero);
    
    cout << "Numero: " << numero << endl;
    
    impNombre(&nombre2);
    cout << "Nombre: " << nombre2 << endl;
    
    return 0;
}

void funcionX(int numeros[], int numeros2[]){
    numeros[0] = 10;
    numeros[1] = 20;
    numeros[2] = 30;

    numeros2[0] = numeros[0];
    numeros2[1] = numeros[1];
    numeros2[2] = numeros[2];
}

void funcionN(int* num){
    *num = 10;
    printf("Posicion No2 = %x\n", num);
}

void impNombre(string* nom){
    *nom = "Nel no es referencia";
}

// Ingresa fecha1
// 10 / 30 / 2


// 1625 - 9999
// 5 unidades
// 2 decenas
// 6 centenas
// 1 unidad millar
//   decena de millar

